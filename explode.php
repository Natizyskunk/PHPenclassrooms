<?php
// Exemple avec explode() et le paramètre limit.
echo '<h2>Exemple avec explode() et le parametre limit.</h2>';
echo '<p>Dans cet exemple on va utiliser: '.'<b>$str ='."'one,two,three,four';</b></p>";


echo '<hr>';
$str = 'one,two,three,four';
// limit 0
print_r(explode(',', $str, 0));
echo '<hr>';

// limit 1
print_r(explode(',', $str, 1));
echo '<hr>';

// limit positif
print_r(explode(',', $str, 2));
echo '<hr>';

// limit négatif (depuis PHP 5.1)
print_r(explode(',', $str, -1));
echo '<hr>';

// limit nul
print_r(explode(',', $str));
echo '<hr>';
?>