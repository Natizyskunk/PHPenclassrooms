<?php
////////////////////
// TIMEZONE HOURS For PARIS, FRANCE
$h = "1";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
$hm = $h * 60;
$ms = $hm * 60;
$gmdate = 'Nous sommes le ' . gmdate("d/m/Y") . ' et il est ' . gmdate("H:i:s", time()+($ms))  .''; // the "-" can be switched to a plus if that's what your time zone is.
echo $gmdate;
////////////////////
?>