<?php 
// Connection à la bdd mysql (PHP My Admin).
try
{
	$db = new PDO('mysql:host=localhost;dbname=phpenclassrooms;charset=utf8', 'root', '');
	// Lorsque le site sera en ligne, il aura sûrement un nom d'hôte différent:
	// $bdd = new PDO('mysql:host=sql.hebergeur.com;dbname=mabase;charset=utf8', 'login', 'password');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}