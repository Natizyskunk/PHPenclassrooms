Au secours ! Mon script plante !<br>
<br>
Alors comme ça votre script ne marche pas, et PHP vous affiche des erreurs incompréhensibles ?<br>
Pas de souci à vous faire : c'est tout à fait normal, on ne réussit jamais un script du premier coup (en tout cas, moi, jamais !).<br>
<br>
Des milliers de messages d'erreur différents peuvent survenir (O.K., jusque-là rien de très rassurant), et je n'ai pas vraiment la possibilité de vous en dresser une liste complète… mais je peux vous présenter les erreurs les plus courantes, ce qui devrait résoudre la grande majorité de vos problèmes. ;-)<br>
<br>
__________________________________________________________________________<br>
__________________________________________________________________________<br>
<br>
<br>
Les erreurs les plus courantes<br>
Je pense qu'il est facile de parler d'erreurs « courantes », car vous verrez que certaines erreurs reviennent plus souvent que d'autres.<br>
<br>
Nous allons passer en revue les erreurs suivantes :<br>
<br>
Parse error ;<br>
Undefined function ;<br>
Wrong parameter count.<br>
Parse error<br>
<br>
Si on devait dire qu'il existe UNE erreur de base, ça serait très certainement celle-là. Impossible de programmer en PHP sans y avoir droit un jour.<br>
Le message d'erreur que vous obtenez ressemble à celui-ci :<br>
<br>
Parse error: parse error in fichier.php on line 15<br>
Ce message vous indique une erreur dansfichier.phpà la ligne 15. Généralement, cela veut dire que votre problème se situe à la ligne 15, mais ce n'est pas toujours le cas (trop facile, sinon). Parfois c'est la ligne précédente qui a un problème : pensez donc à regarder autour de la ligne indiquée.<br>
<br>
Avec un éditeur de texte spécialisé comme Notepad++, vous avez les numéros de ligne sur votre gauche.<br>
<br>
Bon, concrètement, qu'est-ce qu'une parse error ? Une « parse error » est en fait une instruction PHP mal formée. Il peut y avoir plusieurs causes :<br>
<br>
Vous avez oublié le point-virgule à la fin de l'instruction. Comme toutes les instructions doivent se terminer par un point-virgule, si vous oubliez d'en mettre un, ça provoquera une parse error. Par exemple :<br>
$id_news = 5<br>
… génèrera une parse error. Si vous mettez le point-virgule à la fin, tout rentrera dans l'ordre !<br>
<br>
$id_news = 5;<br>
Vous avez oublié de fermer un guillemet (ou une apostrophe, ou une parenthèse). Par exemple :<br>
echo "Bonjour !;<br>
… il suffit de fermer correctement les guillemets et vous n'aurez plus de problème :<br>
<br>
echo "Bonjour !";<br>
Vous vous êtes trompés dans la concaténation, vous avez peut-être oublié un point :<br>
echo "J'ai " . $age " ans";<br>
Une fois l'erreur corrigée, ça donne :<br>
<br>
echo "J'ai " . $age . " ans";<br>
Il peut aussi s'agir d'une accolade mal fermée (pour unif, par exemple). Vérifiez que vous avez correctement fermé toutes vos accolades. Si vous oubliez d'en fermer une, il est probable que la parse error vous indique que l'erreur se trouve à la dernière ligne du fichier (c'est-à-dire à la ligne 115 si votre fichier comporte 115 lignes).<br>
Donc, si on vous indique une erreur à la dernière ligne, il va probablement falloir relire tout le fichier PHP à la recherche d'une accolade mal fermée !<br>
<br>
Si on vous dit que l'erreur est à la ligne 15 et que vous ne voyez vraiment pas d'erreur à cette ligne, n'hésitez pas à chercher l'erreur à la ligne juste au-dessus, elle s'y trouve peut-être !<br>
<br>
Undefined function<br>
<br>
Une autre erreur assez classique : la fonction inconnue. Vous obtenez ce message d'erreur :<br>
<br>
Fatal Error: Call to undefined function: fonction_inconnue() in fichier.php on line 27<br>
Là, il faut comprendre que vous avez utilisé une fonction qui n'existe pas.<br>
<br>
Deux possibilités :<br>
<br>
soit la fonction n'existe vraiment pas. Vous avez probablement fait une faute de frappe, vérifiez si une fonction à l'orthographe similaire existe ;<br>
soit la fonction existe vraiment, mais PHP ne la reconnaît pas. C'est parce que cette fonction se trouve dans une extension de PHP que vous n'avez pas activée. Par exemple, si vous essayez d'utiliser la fonctionimagepngalors que vous n'avez pas activé la bibliothèque GD pour les images en PHP, on vous dira que la fonction n'existe pas. Activez la bibliothèque qui utilise la fonction et tout sera réglé.<br>
Une dernière chose : il se peut aussi que vous essayiez d'utiliser une fonction qui n'est pas disponible dans la version de PHP que vous avez.<br>
Vérifiez dans le manuel dans quelles versions de PHP cette fonction est disponible.<br>
<br>
Wrong parameter count<br>
<br>
Si vous utilisez mal une fonction, vous aurez cette erreur :<br>
<br>
Warning: Wrong parameter count for fonction() in fichier.php on line 112<br>
Cela signifie que vous avez oublié des paramètres pour la fonction, ou même que vous en avez trop mis.<br>
Comme je vous l'ai appris dans le chapitre sur la doc' PHP, consultez le mode d'emploi de la fonction pour savoir combien de paramètres elle prend et quels sont ceux qui sont facultatifs.<br>
<br>
Par exemple, la fonctionfopenrequiert au minimum deux paramètres : le premier pour le nom du fichier à ouvrir et le second pour le mode d'ouverture (en lecture seule, écriture, etc.). Si vous ne mettez que le nom du fichier à ouvrir, comme ceci :<br>
<br>
$fichier = fopen("fichier.txt");<br>
… vous aurez l'erreur « Wrong parameter count ». Pensez donc à rajouter le paramètre qui manque, par exemple comme ceci :<br>
<br>
$fichier = fopen("fichier.txt", "r");<br>
Dans les versions actuelles de PHP, le message d'erreur vous donne même le nombre de paramètres que vous avez oubliés !<br>
<br>
__________________________________________________________________________<br>
__________________________________________________________________________<br>
<br>
<br>
Traiter les erreurs SQL<br>
Comme vous le savez, le langage SQL est un langage à part entière dont on se sert en PHP. S'il peut y avoir des erreurs en PHP, il peut aussi y avoir des erreurs en SQL !<br>
Il se peut par exemple que votre requête soit mal écrite, que la table que vous voulez ouvrir n'existe pas, etc. Bref, les erreurs possibles sont là encore nombreuses.<br>
<br>
Toutefois, ce n'est pas MySQL qui vous dira qu'il y a une erreur, mais PHP, et ce dernier n'est pas très bavard en ce qui concerne les erreurs SQL. Nous allons donc voir :<br>
<br>
comment repérer une erreur SQL en PHP ;<br>
comment faire parler PHP pour qu'il nous donne l'erreur SQL (de gré, ou de force !).<br>
Repérer l'erreur SQL en PHP<br>
<br>
Lorsqu'il s'est produit une erreur SQL, la page affiche le plus souvent l'erreur suivante :<br>
<br>
Fatal error: Call to a member function fetch() on a non-object<br>
<br>
Cette erreur survient lorsque vous voulez afficher les résultats de votre requête, généralement dans la bouclewhile ($donnees = $reponse->fetch()).<br>
<br>
Quand vous avez cette erreur, il ne faut pas chercher plus loin, c'est la requête SQL qui précède qui n'a pas fonctionné. Il vous manque cependant des détails sur ce qui a posé problème dans la requête. Nous allons maintenant voir comment on peut remédier à cela. ;-)<br>
<br>
Allez ! Crache le morceau !<br>
<br>
Comme visiblement PHP n'a pas envie de nous donner l'erreur renvoyée par MySQL, on va le lui demander d'une autre manière. Je vous avais d'ailleurs présenté cette méthode dans un des premiers chapitres sur MySQL.<br>
<br>
Repérez la requête qui selon vous plante (certainement celle juste avant la bouclewhile), et demandez d'afficher l'erreur s'il y en a une, comme ceci :<br>
<br>
<?php
$reponse = $bdd->query('SELECT nom FROM jeux_video') or die(print_r($bdd->errorInfo()));
?><br>
Si la requête fonctionne, aucune erreur ne sera affichée. Si en revanche la requête plante, PHP arrêtera de générer la page et vous affichera l'erreur donnée par MySQL…<br>
<br>
À partir de là, il va falloir vous débrouiller tout seuls, car les erreurs SQL sont assez nombreuses et je ne peux pas toutes les lister.<br>
<br>
En général, MySQL vous dit « You have an error in your SQL syntax near 'XXX' ». À vous de bien relire votre requête SQL ; l'erreur se trouve généralement près de l'endroit où on vous l'indique.<br>
<br>
__________________________________________________________________________<br>
__________________________________________________________________________<br>
<br>
<br>
Quelques erreurs plus rares<br>
Les erreurs PHP sont très variées, et je ne parle même pas des erreurs SQL. N'espérez donc pas que je vous fasse ici la liste des 3946 erreurs de PHP, j'en serais incapable (je ne les ai pas encore toutes eues, mais ça ne saurait tarder à l'allure à laquelle je vais).<br>
<br>
Je vais vous montrer quelques erreurs un peu plus rares que « parse error », mais que vous rencontrerez probablement un jour. Si déjà je peux vous aider pour ces erreurs-là, ce sera bien.<br>
<br>
Nous allons voir les erreurs :<br>
<br>
« Headers already sent by… » ;<br>
« L'image contient des erreurs » ;<br>
« Maximum execution time exceeded ».<br>
Headers already sent by…<br>
<br>
Voilà une erreur classique quand on travaille avec les sessions ou avec les cookies :<br>
<br>
Cannot modify header information - headers already sent by ...<br>
Que doit-on comprendre par là ?<br>
Les headers sont des informations d'en-tête qui sont envoyées avant toute chose au navigateur du visiteur. Elles permettent de dire « Ce que tu vas recevoir est une page HTML », ou « Ce que tu vas recevoir est une image PNG », ou encore « Inscris un cookie ».<br>
Toutes ces choses-là doivent être effectuées avant que le moindre code HTML ne soit envoyé. En PHP, la fonction qui permet d'envoyer des informations de headers s'appelleheader(). On s'en est notamment servi dans le chapitre sur la bibliothèque GD pour indiquer que l'on envoyait une image et non pas une page HTML.<br>
<br>
Il y a d'autres fonctions qui envoient des headers toutes seules. C'est le cas desession_start()et desetcookie().<br>
Ce que vous devez retenir, c'est que chacune des ces fonctions doit être utilisée au tout début de votre code PHP. Il ne faut RIEN mettre avant, sinon ça provoquera l'erreur « Headers already sent by… ».<br>
<br>
Un exemple de code qui génère l'erreur :<br>
<br>
<html>
<?php session_start(); ?><br>
Ici, j'ai eu le malheur de mettre un peu de code HTML avant lesession_start(), et c'est ce qui a provoqué l'erreur. Mettez lesession_start()en tout premier, et vous n'aurez plus de problème :<br>
<br>
<?php session_start(); ?><br>
<html><br>
L'image contient des erreurs<br>
<br>
C'est le navigateur qui vous donne ce message d'erreur et non pas PHP.<br>
Ce message survient lorsque vous travaillez avec la bibliothèque GD. Si vous avez fait une erreur dans votre code (par exemple une banale « parse error »), cette erreur sera inscrite dans l'image. Du coup, l'image ne sera pas valide et le navigateur ne pourra pas l'afficher.<br>
<br>
Bon d'accord, l'erreur est dans l'image. Mais comment faire pour faire « apparaître » l'erreur ?<br>
Deux possibilités :<br>
<br>
vous pouvez supprimer la ligne suivante dans votre code :<br>
<?php header ("Content-type: image/png"); ?><br>
L'erreur apparaîtra à la place du message « L'image contient des erreurs » ;<br>
<br>
vous pouvez aussi afficher le code source de l'image (comme si vous alliez regarder la source HTML de la page, sauf que là il s'agit d'une image).<br>
Dans les deux cas, vous verrez le message d'erreur apparaître. À partir de là, il ne vous restera plus qu'à corriger le bug !<br>
<br>
Maximum execution time exceeded<br>
<br>
Ça, c'est le genre d'erreur qui arrive le plus souvent à cause d'une boucle infinie :<br>
<br>
Fatal error: Maximum execution time exceeded in fichier.php on line 57<br>
Imaginez que vous fassiez une bouclewhile, mais que celle-ci ne s'arrête jamais : votre script PHP va tourner en boucle sans jamais s'arrêter.<br>
<br>
Heureusement, PHP limite le temps d'exécution d'une page PHP à 30 secondes par défaut. Si une page met plus de 30 secondes à se générer, PHP arrête tout en signalant que c'est trop long. Et il fait bien, parce que sinon cela pourrait ralentir tout le serveur et rendre votre site inaccessible !<br>
<br>
Voici un exemple de bouclewhilequi ne s'arrêtera jamais :<br>
<br>
<?php
$nombre = 5;
while ($nombre == 5)
{
    echo 'Zéro ';
}
?><br>
Comme vous pouvez le voir, un tel code PHP ne s'arrêtera jamais parce que$nombrevaut TOUJOURS 5…<br>
<br>
Si vous avez donc l'erreur « Maximum execution time exceeded », il va falloir repérer une boucle qui ne s'arrête jamais, car c'est elle qui provoque ce problème.<br>
<br>
Rassurez-vous : la limite est fixée à 30 secondes, mais vous n'y serez jamais confrontés. En général, un serveur met moins de 50 millisecondes à charger une page PHP (on est très loin des 30 secondes !).