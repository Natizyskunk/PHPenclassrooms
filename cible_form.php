<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Cible</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<p>Bonjour !</p>

<p>- Vous vous appelez : <?php echo strip_tags($_POST['nom']) . '&nbsp;' . strip_tags($_POST['prenom']); ?> !</p>

<p>- Votre pseudo est : <?php echo strip_tags($_POST['pseudo']); ?> !</p>

<p>
    - Voici vos informations supplémentaires : <BR />
    &nbsp;&nbsp;&nbsp;<?php echo strip_tags($_POST['message']); ?> .
</p>

<p>- Vous habitez dans le pays suivant : <?php echo strip_tags($_POST['pays']); ?> .</p>

<p>
    - Vos plats préférés sont les suivants : <BR />
    &nbsp;&nbsp;&nbsp;Steack frites > <?php if (isset($_POST['plat1']) == 1)
    {
        echo 'OUI';
    }
    else
    {
        echo 'NON';
    }
    ?>. <BR />

    &nbsp;&nbsp;&nbsp;Bouchées à la Reine > <?php if (isset($_POST['plat2']) == 1)
    {
        echo 'OUI';
    }
    else
    {
        echo 'NON';
    }
    ?>. <BR />

    &nbsp;&nbsp;&nbsp;Spaghettis bolognaise > <?php if (isset($_POST['plat3']) == 1)
    {
        echo 'OUI';
    }
    else
    {
        echo 'NON';
    }
    ?>. <BR />

    &nbsp;&nbsp;&nbsp;Rôti de boeuf >  <?php if (isset($_POST['plat4']) == 1)
    {
        echo 'OUI';
    }
    else
    {
        echo 'NON';
    }
    ?>. <BR />
</p>

<p>- Vous aimez les ballades en fôret : <?php echo strip_tags($_POST['ballades']); ?> .</p>

<BR />
<BR />

<p>Si vous désirez changer vos informations, <a href="form.php">clique ici</a> pour revenir à la page formulaire.</p>

<p>
    <a href="index.php">Retour accueil !</a>
</p>

</body>
</html>