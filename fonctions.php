<?php
$phrase = 'Bonjour tout le monde ! Je suis une phrase !';
$longueur = strlen($phrase);  /*strlen: longueur d'une chaîne*/


echo 'La phrase ci-dessous comporte ' . $longueur . ' caractères :<br />' . '>&nbsp;' . $phrase;
?>

    <br>
    <br>

<?php
$ma_variable = str_replace('b', 'p', 'bim bam boum');  /*str_replace: remplace une chaîne de caractères par une autre.*/

echo $ma_variable;
?>

    <br>
    <br>

<?php
$chaine = 'Cette chaine va etre melangee !';
$chaine = str_shuffle($chaine);

echo $chaine;
?>

    <br>
    <br>

<?php
$chaine = 'COMMENT CA JE CRIE TROP FORT ???';
$chaine = strtolower($chaine); /*strtolower: met tous les caractères d'une chaîne en minuscules.*/

echo $chaine;
?>

    <br>
    <br>

<?php
$phrase = 'Bonjour je suis une phrase un peu plus longue.';
//$nombredeCaracteres = strlen($phrase);

/*$phraseMelangee = str_shuffle($phrase)
echo $phraseMelangee;*/
echo str_shuffle($phrase);

//echo 'Il y a '.$nombredeCaracteres.' caractères dans cette phrase.';
?>

    <br>
    <br>

<?php
//POUR LES DATES
$jour = date('d');
$mois = date('m');
$annee = date('Y');
$heure = date('H');
$minute = date('i');

echo 'Bonjour ! Nous sommes le '.$jour.'/'.$mois.'/'.$annee.'et il est '.$heure.'h'.$minute.'.';
//echo date('d/m/y');
?>

    <br>
    <br>

<?php
//CREER SES PROPRES FONCTIONS
function direBonjour($nom)
{
    echo '<p>Bonjour '.$nom.'</p>';
}

direBonjour('Marie');
direBonjour('Martin');
direBonjour('Mathieu');
?>

    <br>
    <br>

<?php
// Ci-dessous, la fonction qui calcule le volume du cône
function VolumeCone($rayon, $hauteur)
{
    $volume = $rayon * $rayon * 3.14 * $hauteur * (1/3); // calcul du volume
    return $volume; // indique la valeur à renvoyer, ici le volume
}

$volume = VolumeCone(3, 1);
echo 'Le volume d\'un cône de rayon 3 et de hauteur 1 est de ' . $volume;
?>