<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Cible_envoi</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<nav>
    <p id="recapitulatif">R&eacute;capitulatif de votre upload.</p>
</nav>


<div>
    <?php
    //VARIABLES

    // Testons si l'icone a bien été envoyée et s'il n'y a pas d'erreur.
    if (isset($_FILES['icone_fichier']) AND $_FILES['icone_fichier']['error'] == 0)
    {
        // Testons si le fichier n'est pas trop gros
        if ($_FILES['icone_fichier']['size'] <= 1000000)
        {
            // Testons si l'extension est autorisée
            $infosfichier = pathinfo($_FILES['icone_fichier']['name']);
            $extension_upload = $infosfichier['extension'];
            $extensions_autorisees = array('jpg', 'jpeg', 'png', 'gif');
            $name = $infosfichier['filename'];

            //Récupérons les informations personnelles (Nom et Prénom)
            $prenom = $_POST['personal_informations_prenom'];
            $nom = $_POST['personal_informations_nom'];

            //définir dossier d'upload 'uploads/Nom.Prénom/'
            $path="uploads/$nom.$prenom/";
            // Vérifier si le dossier existe déjà ou pas.
            if (file_exists($path)) // s'il existe
            {
                //Ne rien faire.
            }
            else //Si le dossier n'existe pas encore.
            {
                //Créer un dossier 'uploads/Nom.Prénom/'
                mkdir('uploads/'. $nom . '.' . $prenom . '/', 0777, true);
            }

            // TIMEZONE HOURS For PARIS, FRANCE
            $h = "1";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
            $hm = $h * 60;
            $ms = $hm * 60;
            $gmdate = gmdate("d.m.Y.H\hi", time()+($ms)); // the "-" can be switched to a plus if that's what your time zone is.

            $file = $name . '_-_' . $gmdate . '.' .$extension_upload;
            /*$file = $name .'_-_' . $nom . '.' . $prenom . '_' . $gmdate . '.' .$extension_upload;*/

            if (in_array($extension_upload, $extensions_autorisees))
            {
                // Testons si l'image respecte les dimensions imposées.
                $maxwidth = 1920;
                $maxheight = 1080;
                $image_sizes = getimagesize($_FILES['icone_fichier']['tmp_name']);
                if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight)
                {
                    echo "- image trop grande. <BR />";
                }
                elseif ($image_sizes[0] <= $maxwidth OR $image_sizes[1] <= $maxheight)
                {
                    echo "image à la bonne taille. <BR /><BR />";
                    {
                        // On peut valider le fichier et le stocker définitivement
                        move_uploaded_file(($_FILES['icone_fichier']['tmp_name']), $path . $file /*basename($_FILES['icone_fichier']['name'])*/);
                        echo "- L'envoi de votre icone a bien été effectué ! <BR />";
                        echo '<img src="'.$path . $file.'" alt="icon" height="112" width="200">';
                        /*echo '<img src="' . 'uploads/' . ($_FILES['icone_fichier']['name']).'"  alt="icone" height="112" width="200">';*/
                    }
                }
            }
        }
    }
    else
    {
        echo "- Erreur lors du transfert de votre icone.";
    }

    ?>

    <BR />
    <BR />

    <?php
    // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur.
    if (isset($_FILES['mon_fichier']) AND $_FILES['mon_fichier']['error'] == 0)
    {
        // Testons si le fichier n'est pas trop gros
        if ($_FILES['mon_fichier']['size'] <= 4000000)
        {
            // Testons si l'extension est autorisée
            $infosfichier = pathinfo($_FILES['mon_fichier']['name']);
            $extension_upload = $infosfichier['extension'];
            $extensions_autorisees = array('txt', 'doc', 'docx');
            $name = $infosfichier['filename'];
            $prenom = $_POST['personal_informations_prenom'];
            $nom = $_POST['personal_informations_nom'];
            ////////////////////
            //Créer un dossier 'uploads/Nom.Prénom/'
            //définir dossier d'upload 'uploads/Nom.Prénom/'
            $path="uploads/$nom.$prenom/";
            // Vérifier si le dossier existe déjà ou pas.
            if (file_exists($path))
            {
                //Ne rien faire.
            }
            else //Si le dossier n'existe pas encore.
            {
                //Créer un dossier 'uploads/Nom.Prénom/'
                mkdir("uploads/$nom.$prenom/", 0777, true);
                /*mkdir('uploads/'. $nom . '.' . $prenom . '/', 0777, true);*/
            }
            ////////////////////
            // TIMEZONE HOURS For PARIS, FRANCE
            $h = "1";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
            $hm = $h * 60;
            $ms = $hm * 60;
            $gmdate = gmdate("d.m.Y.H\hi", time()+($ms)); // the "-" can be switched to a plus if that's what your time zone is.
            ////////////////////
            $file = $name . '_-_' . $gmdate . '.' .$extension_upload;

            if (in_array($extension_upload, $extensions_autorisees))
            {
                // On peut valider le fichier et le stocker définitivement
                $titre = $_POST['titre'];
                $description = $_POST['description'];
                move_uploaded_file($_FILES['mon_fichier']['tmp_name'], $path . $file /*basename($_FILES['mon_fichier']['name'])*/);
                echo "- L'envoi de votre fichier \"$file\" a bien été effectué ! <BR /><BR />";
                echo "- Vous lui avez donné le titre suivant : $titre <BR /><BR /> ";
                echo "- Et vous  avez indiqué la description suivante : <BR /> $description";
            }
        }
    }
    else
    {
        echo "- Erreur lors du transfert de votre fichier.<BR />";
    }
    ?>
</div>

<BR />

<div>
    <p>Si vous désirez changer vos informations, <a href="upload.php" id="upload_page_button">cliquez ici</a> pour revenir à la page upload.</p>

    <BR />

    <p> <a href="index.php" id="homepage_button">Retour accueil !</a> </p>
</div>

</body>
</html>