<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Form</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<form action="cible_form.php" method="post">
    <p>Veuillez taper votre nom et prénom :</p>
    <p>
        <input type="text" name="nom" placeholder="Votre nom." />
        <input type="text" name="prenom" placeholder="Votre prénom." />
    </p>

    <BR />

    <p>Vous pouvez indiquer ici des informtations supplémentaires vous concernant :</p>
    <textarea name="message" placeholder="Vos infos ici." rows="8" cols="40"></textarea>

    <BR />
    <BR />

    <p>Dans quel pays habitez vous ?</p>
        <select name="pays">
            <option value="France" selected="selected">France </option>
            <option value="Allemagne">Allemagne</option>
            <option value="Suisse">Suisse</option>
            <option value="Grande-Bretagne">Grande-Bretagne</option>
        </select>

    <BR />
    <BR />

    <p>Quels sont vos plats préférés ?</p> <!-- Si vous voulez que la case soit cochée par défaut, il faudra lui rajouter l'attribut checked="checked" -->
    <input type="checkbox" name="plat1" id="plat1" /> <label for="plat1">Steack frites</label> <BR/>
    <input type="checkbox" name="plat2" id="plat2" /> <label for="plat2">Bouchées à la Reine</label> <BR/>
    <input type="checkbox" name="plat3" id="plat3" /> <label for="plat3">Spaghettis bolognaise</label> <BR/>
    <input type="checkbox" name="plat4" id="plat4" /> <label for="plat4">Rôti de boeuf</label>

    <BR />
    <BR />

    <p>Aimez-vous les ballades en fôret ?</p> <!-- Si vous voulez que la case soit cochée par défaut, il faudra lui rajouter l'attribut checked="checked" -->
    <input type="radio" name="ballades" value="oui" id="oui" /> <label for="oui">Oui</label>
    <input type="radio" name="ballades" value="non" id="non" /> <label for="non">Non</label>

    <BR />
    <BR />

    <p> <input type="submit" name="envoi_formulaire" id="envoi_formulaire" value="Envoyer le formulaire"/> </p>

    <BR />

    <p>
        <a href="index.php" id="homepage_button"> Retour accueil !</a>
    </p>

    <p> <!-- Les champs cachés -->
        <input type="hidden" name="pseudo" value="nfourie" />
    </p>
</form>

</body>
</html>