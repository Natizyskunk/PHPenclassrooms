<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>TITRE</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<?php


//WHILE
$repetitions = 0;

while ($repetitions < 10)
{
    echo '<p>Je ne dois pas copier sur mon voision. ' .$repetitions. ' fois.</p>';
    $repetitions++;  // $repetitions = $repetitions = 1
}

?>

<br>
<br>

<?php

//FOR
for ($repetitions = 0 ; $repetitions < 10 ; $repetitions++)
{
    echo '<p>Je ne dois pas copier sur mon voision. ' .$repetitions. ' fois.</p>';
}

?>

</body>
</html>