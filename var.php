<?php

//VARIABLES

$ageduvisiteur = 20;
// echo $ageduvisiteur.'<br>';

$NULL = NULL;

$prix = 2.5;
$quantite = 10;

$message = 'bonjour !';

$estCeVrai = true;

$estCeFaux = false;


//***********************************


// TEXTE

echo 'le visiteur à '.$ageduvisiteur.' ans.<br>';

//calculs
$total = $prix * $quantite;
echo 'le prix de cet appareil est de '.($total + 5) .'€ .';

?><br>
<br>
__________________________________________________<br>
<br>
<br>
Des noms de variables peu clairs<br>
<br>
<?php
$mess_page = 20;

$ret = $bdd->query('SELECT COUNT(*) AS nb FROM livre');

$data = $ret->fetch();
$total = $data['nb'];

$nb_total  = ceil($total / $mess_page);

echo 'Page : ';
for ($i = 1 ; $i <= $nb_total ; $i++)
{
    echo '<a href="livre.php?page=' . $i . '">' . $i . '</a> ';
}

?><br>
<br>
__________________________________________________<br>
<br>
<br>
Des noms de variables beaucoup plus clairs<br>
<br>
<?php
$nombreDeMessagesParPage = 20;

$retour = $bdd->query('SELECT COUNT(*) AS nb_messages FROM livre');
$donnees = $retour->fetch();
$totalDesMessages = $donnees['nb_messages'];

$nombreDePages  = ceil($totalDesMessages / $nombreDeMessagesParPage);

echo 'Page : ';
for ($page_actuelle = 1 ; $page_actuelle <= $nombreDePages ; $page_actuelle++)
{
    echo '<a href="livre.php?page=' . $page_actuelle . '">' . $page_actuelle . '</a> ';
}
?><br>
C'est fou comme des noms écrits correctement en français permettent d'y voir plus clair.