<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>TITRE</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<?php

//Prénoms
$prenoms = array(
    'Mathieu',
    'Marie',
    'Robert');

echo $prenoms[1]; //pour afficher le prénom 'Marie'
/*print_r($prenoms);*/ //Pour afficher le contenu entier d'un tableau

//FOR & FOREACH
for ($numero = 0; $numero < 3; $numero++)
{
    echo '<p>'.$prenoms[$numero].'</p>';
}

foreach ($prenoms as $nom)
{
    echo '<p>'.$nom.'</p>';
}


echo '<br>';
echo '<br>';


//Personne
$coordonnees = array(
    'nom' => 'Fourié',
    'prenom' => 'Natan',
    'age' => 19,
    'ville' => 'Strasbourg');
/*print_r($personne);*/

foreach ($coordonnees as $libelle => $detail)
{
    echo '<p>' . $libelle . ' vaut ' . $detail . '</p>';
}

//Vérifier si une clé existe dans l'array: array_key_exists
if (array_key_exists('nom', $coordonnees))
{
    echo 'La clé "nom" se trouve dans les coordonnées !';
}


echo '<br>';
echo '<br>';


//fruits
$fruits = array ('Banane', 'Pomme', 'Poire', 'Cerise', 'Fraise', 'Framboise');

//Vérifier si une valeur existe dans l'array : in_array
if (in_array('Cerise', $fruits))
{
    echo 'La valeur "Cerise" se trouve dans les fruits !<br />';
}

//Récupérer la clé d'une valeur dans l'array : array_search
$position = array_search('Fraise', $fruits);
echo '"Fraise" se trouve en position ' . $position . '<br />';

$position = array_search('Banane', $fruits);
echo '"Banane" se trouve en position ' . $position;

?>

</body>
</html>