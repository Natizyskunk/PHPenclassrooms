<?php
$array1 = array("a" => "green", "red", "blue", "black");
$array2 = array("b" => "green", "yellow", "red", "purple");
$result1 = array_diff($array1, $array2);
$result2 = array_diff($array2, $array1);

$PrefixDisplayArray1 = '<b>$array1:</b>';
$PrefixDisplayArray2 = '<b>$array2:</b>';
$DisplayArray1 = array();
$DisplayArray2 = array();

echo $PrefixDisplayArray1 . ' '; print_r($array1);
echo '<hr>';
echo $PrefixDisplayArray2 . ' '; print_r($array2);
echo '<hr>';
echo '<hr>';
echo '<b>Différences:</b>' . ' '; print_r($result1);
echo '<hr>';
echo '<b>Différences:</b>' . ' '; print_r($result2);
?>