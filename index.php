<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mon super site</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<!-- L'en-tête -->
<?php include("header.php"); ?>


<!-- Le menu -->
<?php include("menu.php"); ?>

<!-- Le corps -->
<div id="corps">
    <h1>Mon super site - Accueil</h1>

    <p>
        Bienvenue sur mon super site !<BR />
        Vous allez adorer ici, c'est un site génial qui va parler de... euh... Je cherche encore un peu le thème de mon site. :-D
    </p>
    <p>
        Cette ligne a été écrite entièrement en HTML.<br />
        <br />
        <?php echo "Cette ligne a été écrite \"uniquement\" en PHP.";?><br />
        <br />
        <?php
        // Cette ligne indique où j'habite
        echo "J'habite en Chine et ";

        // La ligne suivante indique mon âge
        echo "j'ai 92 ans.";
        ?>
    </p>
</div>

<BR />

<p> <!-- Envoyer des paramètres dans l'URL -->
<a href="bonjour.php?nom=Dupont&amp;prenom=Jean">Dis-moi bonjour !</a>
</p>

<p>
    <a href="form.php">Test formulaire !</a>
</p>

<p>
    <a href="upload.php" id="index_upload_button"> Uploader un fichier !</a>
</p>

<BR />

<!-- Le pied de page -->
<?php include("footer.php"); ?>

</body>
</html>