<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>TITRE</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<!-- Envoyer des paramètres dans l'URL depuis 'index.php' vers -> 'bonjour.php' et Récupérer les paramètres en PHP -->
<p>Bonjour <?php echo $_GET['prenom'] . ' ' . $_GET['nom']; ?> !</p>


<!-- Tester la présence d'un paramètre dans l'URL -->
<?php
if (isset($_GET['prenom']) AND isset($_GET['nom'])) // On a le nom et le prénom
{
    echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !';
}
else // Il manque des paramètres, on avertit le visiteur
{
    echo 'Il faut renseigner un nom et un prénom !';
}
?>


<!-- Contrôler la valeur des paramètres -->
<?php
if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter']))
{
    for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
    {
        echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
    }
}
else
{
    echo 'Il faut renseigner un nom, un prénom et un nombre de répétitions !';
}
?>

<!-- résultat final -->
<?php
if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter']))
{
    // 1 : On force la conversion en nombre entier
    $_GET['repeter'] = (int) $_GET['repeter'];

    // 2 : Le nombre doit être compris entre 1 et 100
    if ($_GET['repeter'] >= 1 AND $_GET['repeter'] <= 100)
    {
        for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
        {
            echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
        }
    }
}
else
{
    echo 'Il faut renseigner un nom, un prénom et un nombre de répétitions !';
}
?>

<p>
    <a href="index.php" id="homepage_button"> Retour accueil !</a>
</p>

</body>
</html>