<?php
// Connection et liaison à la bdd mysql (PHP My Admin).
include("../assets/PDO/PDOconnect.php");


// Chargement de la classe PHP A pour lier les champs de la bdd sql.
require_once '../assets/class/A.class.php';

// Chargement de la classe PHP A pour lier les champs de la bdd sql.
require_once '../assets/class/PersonnagesManager.class.php';

// Chargement de la classe PHP A pour lier les champs de la bdd sql.
require_once '../assets/class/Personnage.class.php';


// On admet que $db est un objet PDO.
$request = $db->query('SELECT id, nom, forcePerso, degats, niveau, experience FROM personnages');
    

/* while ($donnees = $request->fetch(PDO::FETCH_ASSOC)) // Chaque entrée sera récupérée et placée dans un array.
{
  // On passe les données (stockées dans un tableau) concernant le personnage au constructeur de la classe.
  // On admet que le constructeur de la classe appelle chaque setter pour assigner les valeurs qu'on lui a données aux attributs correspondants.
  $perso = new Personnage($donnees);
        
  // echo $perso->nom();
  echo $perso->nom(), ' a ', $perso->forcePerso(), ' de force, ', $perso->degats(), ' de dégâts, ', $perso->experience(), ' d\'expérience et est au niveau ', $perso->niveau(), '<br>';
} */

// On créé un nouveau personnage ($perso1) en lui indiquant ses différents paramètres.
$perso1 = new Personnage([
  'nom' => 'Victor',
  'forcePerso' => 40,
  'degats' => 60,
  'niveau' => 6,
  'experience' => 40
]);

// On créé un nouveau personnage ($perso2) en lui indiquant ses différents paramètres.
$perso2 = new Personnage([
  'nom' => 'Henry',
  'forcePerso' => 50,
  'degats' => 75,
  'niveau' => 8,
  'experience' => 50
]);


// DEBUG
// echo '<br> DEBUG : ' . $perso->nom();


$manager = new PersonnagesManager($db);    
$manager->add($perso1);
$manager->add($perso2);


echo '<br>';
$a = new A;
$method = 'hello';
$a->$method(); // Affiche : « Hello world ! »