<?php


// TEST1
$age = 10 ;

if ($age < 12) // > ; < ; >= ; <= ; == ; !=
{
    echo 'salut gamin <br> ';
    echo 'salut gamin '.$age.' ans.<br>';

}
elseif ($age == 14)
{
    echo 'Tu as 14 ans <br>';
}
else
{
    echo 'Bonjour cher adulte ! <br>';
}



// TEST2
$adulte = true;
$nom= "Bernard";

if ($adulte)  // !$adulte veut dire "si ça n'est pas un 'adulte' "
{
    echo 'tu es un adulte <br>';
}
else
{
    echo 'Tu es un enfant <br>';
}

switch ($age)
{
    case 4:
        echo 'T as 4 ans';
        break;
    case 16:
        echo 'Tu es un peu plus âgé, tu as 16 ans.';
        break;
    case 18:
        echo 'Tu es majeur !';
        break;
}

?>

<p>Je coupe mon code php en 2 avec une balise html</p>

<?php

if ($adulte AND $nom == "Bernard")  // AND ou && ; OR ou ||
{

}
?>

<?php
// IF Ternaire
// Exemple d'utilisation pour l'opérateur ternaire
$action = (empty($_POST['action'])) ? 'default' : $_POST['action'];

// La ligne ci-dessus est identique à la condition suivante :
if (empty($_POST['action'])) {
   $action = 'default';
} else {
   $action = $_POST['action'];
}
?>