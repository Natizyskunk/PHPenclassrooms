<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Upload</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<form action="cible_upload.php" method="post"  enctype="multipart/form-data">
    <p>
        <h3>Vous désirez envoyer des fichiers ? :</h3> <BR />
        <label for="personal_informations">Informations personnelles :</label> <br />
    <label for="personal_informations_nom">Nom :</label> <input type="text" name="personal_informations_nom" placeholder="Votre nom" id="personal_informations_nom" /> &nbsp;&nbsp; <label for="personal_informations_prenom">Pr&eacute;nom :</label> <input type="text" name="personal_informations_prenom" placeholder="Votre pr&eacute;nom" id="prenom" /> <br />
        <BR />
        <label for="icone_fichier">Icône du fichier (JPG/JPEG, PNG ou GIF | max. 1 Mo | Dimensions max. 1920x1080) :</label> <br />
        <input type="file" name="icone_fichier" id="icone_fichier" /> <br />
        <BR />
        <label for="mon_fichier">Fichier (TXT, DOC, DOCX | max. 4 Mo) :</label> <br />
        <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
        <input type="file" name="mon_fichier" id="mon_fichier"/> <br />
        <BR />
        <label for="titre">Titre du fichier (max. 50 caractères) :</label> <br />
        <input type="text" name="titre" placeholder="Titre du fichier" id="titre" /> <br />
        <BR />
        <label for="description">Description de votre fichier (max. 255 caractères) :</label> <br />
        <textarea name="description" id="description"  placeholder="Description..." rows="8" cols="40"></textarea> <br />
        <BR />
        <input type="submit" name="envoi_fichier" id="envoi_fichier" value="Envoyer le fichier"/>
    </p>
</form>

<BR />

<p>
    <a href="index.php" id="homepage_button"> Retour accueil !</a>
</p>

</body>
</html>